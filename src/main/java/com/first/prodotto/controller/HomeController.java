package com.first.prodotto.controller;

import com.first.prodotto.model.Prodotto;
import com.netflix.discovery.DiscoveryClient;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
public class HomeController {

//    private final RestTemplate restTemplate;
//    private final DiscoveryClient discoveryClient;

    @GetMapping("/prodotto/{idProdotto}")
    public Prodotto getProdotto(@PathVariable Integer idProdotto) {
        Prodotto prodotto1 = new Prodotto();
        prodotto1.setIdProdotto(10);
        prodotto1.setPrezzoUnitario(1012);
        Prodotto prodotto2 = new Prodotto();
        prodotto2.setIdProdotto(11);
        prodotto2.setPrezzoUnitario(1112);
        Prodotto prodotto3 = new Prodotto();
        prodotto3.setIdProdotto(12);
        prodotto3.setPrezzoUnitario(1212);
        List<Prodotto> prodotti = Arrays.asList(prodotto1, prodotto2, prodotto3);
        Optional<Prodotto> prodottoOpt = prodotti.stream().filter(item -> item.getIdProdotto().compareTo(idProdotto) == 0).findFirst();
        return prodottoOpt.orElse(null);
    }
}

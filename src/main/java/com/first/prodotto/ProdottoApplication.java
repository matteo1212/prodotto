package com.first.prodotto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProdottoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProdottoApplication.class, args);
    }

}

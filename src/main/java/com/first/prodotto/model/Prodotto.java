package com.first.prodotto.model;

import lombok.Data;

@Data
public class Prodotto {

    private Integer idProdotto;
    private Integer prezzoUnitario;
}

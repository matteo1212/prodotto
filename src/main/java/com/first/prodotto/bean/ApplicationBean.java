package com.first.prodotto.bean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Configuration
//@ComponentScan(basePackageClasses = Persona.class)
public class ApplicationBean {

    @Bean
    public RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
